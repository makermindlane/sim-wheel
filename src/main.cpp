#include <Arduino.h>

// Rotary Encoder Inputs
#include <Encoder.h>

#include "Joystick.h"

// Change these two numbers to the pins connected to your encoder.
//   Best Performance: both pins have interrupt capability
//   Good Performance: only the first pin has interrupt capability
//   Low Performance:  neither pin has interrupt capability
Encoder wheel(2, 3);
Encoder accel(4, 5);
Encoder brake(6, 7);
Encoder clutch(8, 9);

Joystick_ Joystick(JOYSTICK_DEFAULT_REPORT_ID, JOYSTICK_TYPE_MULTI_AXIS, 0, 0, false, false, true,
                   false, false, false, false, false, true, true, true);

const uint8_t MAX_RANGE = 100;

void setup() {
    Joystick.setAcceleratorRange(0, MAX_RANGE);
    Joystick.setBrakeRange(0, MAX_RANGE);
    Joystick.setSteeringRange(0, MAX_RANGE);
    // Joystick.setXAxisRange(0, MAX_RANGE);
    // Joystick.setYAxisRange(0, MAX_RANGE);
    Joystick.setZAxisRange(0, MAX_RANGE);
    // Joystick.setThrottleRange(0, MAX_RANGE);
    Joystick.begin(false);

    // Serial.begin(9600);
    // Serial.println("Basic Encoder Test:");
}

int32_t old_pos_steering = -999;
int32_t old_pos_accel = -999;
int32_t old_pos_brake = -999;
int32_t old_pos_clutch = -999;

void loop() {
    int32_t new_pos_steering = wheel.read();
    int32_t new_pos_accel = accel.read();
    int32_t new_pos_brake = brake.read();
    int32_t new_pos_clutch = clutch.read();

    if (new_pos_steering > MAX_RANGE) {
        new_pos_steering = MAX_RANGE;
        wheel.write(new_pos_steering);
    } else if (new_pos_steering < 0) {
        new_pos_steering = 0;
        wheel.write(new_pos_steering);
    }

    if (new_pos_accel > MAX_RANGE) {
        new_pos_accel = MAX_RANGE;
        accel.write(new_pos_accel);
    } else if (new_pos_accel < 0) {
        new_pos_accel = 0;
        accel.write(new_pos_accel);
    }

    if (new_pos_brake > MAX_RANGE) {
        new_pos_brake = MAX_RANGE;
        brake.write(new_pos_brake);
    } else if (new_pos_brake < 0) {
        new_pos_brake = 0;
        brake.write(new_pos_brake);
    }

    if (new_pos_clutch > MAX_RANGE) {
        new_pos_clutch = MAX_RANGE;
        clutch.write(new_pos_clutch);
    } else if (new_pos_clutch < 0) {
        new_pos_clutch = 0;
        clutch.write(new_pos_clutch);
    }

    if (new_pos_steering != old_pos_steering) {
        old_pos_steering = new_pos_steering;
        // Serial.print("steering: ");
        // Serial.println(new_pos_steering);
    }

    if (new_pos_accel != old_pos_accel) {
        old_pos_accel = new_pos_accel;
        // Serial.print("accel: ");
        // Serial.println(new_pos_accel);
    }

    if (new_pos_brake != old_pos_brake) {
        old_pos_brake = new_pos_brake;
        // Serial.print("brake: ");
        // Serial.println(new_pos_brake);
    }
    if (new_pos_clutch != old_pos_clutch) {
        old_pos_clutch = new_pos_clutch;
        // Serial.print("clutch: ");
        // Serial.println(new_pos_clutch);
    }

    Joystick.setSteering(new_pos_steering);
    Joystick.setAccelerator(new_pos_accel);
    Joystick.setBrake(new_pos_brake);

    // Joystick.setXAxis(new_pos_steering);
    // Joystick.setYAxis(new_pos_accel);
    Joystick.setZAxis(new_pos_clutch);
    // Joystick.setThrottle(new_pos_clutch);
    Joystick.sendState();
    delay(1);
}
